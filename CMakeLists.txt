cmake_minimum_required(VERSION 3.21)
project(PLD7_6a)

set(CMAKE_CXX_STANDARD 14)

add_executable(PLD7_6a Project9PLD7-6a.cpp)
