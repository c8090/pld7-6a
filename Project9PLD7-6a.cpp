#include <iostream>
#include <fstream>
#include <cstdlib>

// Pseudocode PLD Chapter 7 #6a pg. 301
// Name: Jonathan McGaha
// Date: 03/25/2022
//
// Start
//     Declarations
//         InputFile masterFile;
//         InputFile transactionFile;
//         OutputFile newMasterFile;
//         num mClientNumber, mtotalClientCost, tClientNumber, titemClientCost
//         string mClientfName, mClientlName
//     output "Master File Updating Starting"
//     open masterFile "Master.rtf"
//     open transactionFile "Transaction.rtf"
//     open newMasterFile "newMaster.rtf"
//     read mClientNumber, mClientfName, mClientlName, mtotalClientCost from masterFile
//     read tClientNumber, titemClientCost from transactionFile
//     while ( transactionFile not EOF )
//         while (( masterFile not EOF) and (mClientNumber < tClientNumber))
//             output mClientNumber, mClientfName, mClientlName, mtotalClientCost to newMasterFile
//             read mClientNumber, mClientfName, mClientlName, mtotalClientCost from masterFile
//         endwhile
//         if (masterFile is EOF)
//             output "Error Client ID: ", tClientNumber, " not in Master File."
//         else if (mClientNumber == tClientNumber) then
//             mtotalClientCost = mtotalClientCost + titemClientCost
//             output mClientNumber, mClientfName, mClientlName, mtotalClientCost to newMasterFile
//             read mClientNumber, mClientfName, mClientlName, mtotalClientCost from masterFile
//         else if (mClientNumber > tClientNumber) then
//             output "Error Client ID: ", tClientNumber, " not in Master File."
//         endif
//         read tClientNumber, titemClientCost from transactionFile
//     endwhile
//     while (masterFile not EOF)
//         output mClientNumber, mClientfName, mClientlName, mtotalClientCost to newMasterFile
//         read mClientNumber, mClientfName, mClientlName, mtotalClientCost from masterFile
//     endwhile
//     output "Master File Updating Complete"
//     close masterFile
//     close transactionFile
//     close newMasterFile
// Stop


using namespace std;

int main() {
    ifstream masterFile;
    ifstream transactionFile;
    ofstream newMasterFile;
    double mClientNumber, mtotalClientCost, tClientNumber, titemClientCost;
    string mClientfName, mClientlname;

    cout << "Master file updating starting" << endl;
    masterFile.open("Master.rtf");
    transactionFile.open("Transaction.rtf");
    newMasterFile.open("newMaster.rtf");
    masterFile >> mClientNumber;
    masterFile >> mClientfName;
    masterFile >> mClientlname;
    masterFile >> mtotalClientCost;
    transactionFile >> tClientNumber;
    transactionFile >> titemClientCost;
    newMasterFile << "Client # | Client Name | Weekly $$\n";
    while (!(transactionFile.eof())) {
        while ((!(masterFile.eof())) && (mClientNumber < tClientNumber)) {
            newMasterFile << "   " << mClientNumber << "     ";
            newMasterFile << mClientfName << " ";
            newMasterFile << mClientlname << "      ";
            newMasterFile << mtotalClientCost << endl;
            masterFile >> mClientNumber;
            masterFile >> mClientfName;
            masterFile >> mClientlname;
            masterFile >> mtotalClientCost;
        }
        if (masterFile.eof()) {
            cout << "Error Client ID: " << tClientNumber << " not in Master File." << endl;
        } else if (mClientNumber == tClientNumber) {
            mtotalClientCost = mtotalClientCost + titemClientCost;
            newMasterFile << "   " << mClientNumber << "     ";
            newMasterFile << mClientfName << " ";
            newMasterFile << mClientlname << "      ";
            newMasterFile << mtotalClientCost << endl;
            masterFile >> mClientNumber;
            masterFile >> mClientfName;
            masterFile >> mClientlname;
            masterFile >> mtotalClientCost;
        } else if (mClientNumber > tClientNumber) {
            cout << "Error Client ID: " << tClientNumber << " not in Master File." << endl;
        }
        transactionFile >> tClientNumber;
        transactionFile >> titemClientCost;
    }
    while (!(masterFile.eof())) {
        newMasterFile << "   " << mClientNumber << "     ";
        newMasterFile << mClientfName << " ";
        newMasterFile << mClientlname  << "      ";
        newMasterFile << mtotalClientCost << endl;
        masterFile >> mClientNumber;
        masterFile >> mClientfName;
        masterFile >> mClientlname;
        masterFile >> mtotalClientCost;
    }

    cout << "Master File Update Complete" << endl;
    masterFile.close();
    transactionFile.close();
    newMasterFile.close();
    system("PAUSE");
    return 0;
}
